# Getting started   

I'd recommend using Rstudio for developing R. 

1. Install R [https://www.r-project.org/]
2. Install Rstudio following the instructions [here](https://www.rstudio.com/products/rstudio/download2/).


### RStudio 

* Editor
* Console
* Explorer
* Output

*Recommended Rstudio Settings:*
Under *Tools*->*Global Options* unselect the options: "Always save history", and "Restore .RData into workplace at startup". 

These settings will let you start a new R session without data and variables saved from a previoussession loaded into the global environment. 

*Useful shortcuts*

* Ctrl + Enter : (in editor) run highlighted lines in console
* Ctrl + uparrow : (in editor) See previous commands run in console

*Saving your session*

I'd recommend using the editor for running most commands. Particularly when starting, saving commands from a session is a useful way to review what you've learned. 

# Approach to R

For these notes I'll be presenting the approach to `R` associated with packages in the _tidyverse_. These are a series of packages aimed at making data cleaning, transformations, filtering and visualization easier to do. 

<!-- 
Hadley Wickahm
There are also a  
number of free online tutorials associated with
this approach
-->

### Packages 

The command to install new packages in `R`

```
install.packages("dplyr")

```

Recommend packages from 
[R for Data Science](todo)

```
pkgs <- c("broom", "dplyr", "ggplot2", "jsonlite", "stringr", "tibble", "tidyr" )

```

Packages are downloaded from a central repository, CRAN. Packages can installed directly or installed from a site like github using the `devtools` package.


To load a packages use
```
library(dplyr)

```

Once a package loaded datasets and commands associated with the package are available. 

### Help

All CRAN packages should have some documentation which usually can be found online. 

If you know the name of a command (or a dataset) you can see the usage by typing a question mark before the function name/dataset: 

```
?mean

```

You can also look at the contents of a package

```
library(help = dplyr)
 
```

# Basic data types 

Most data types in `R` can be though of has

1. Vectors or arrays of a single data type
2. Lists which contain multiple data types

For more on data types try Hadley Wickham's [Advanced R](https://adv-R.had.co.nz). (Many of the examples and notes below are taken from this book.)

### Primitive types

Vectors can be of several primitive types. These are the main types

1. Double
2. Integer
3. Character
4. Factor
5. Logical

Usage

```
  # double
  vec <- c(0,1,2,3,4)
  length(vec)
  typeof(vec)
  is.numeric(vec)

  # character
  char_vec <- c("a", "b", "c")
  length(char_vec)
  typeof(char_vec)
  is.character(char_vec)

  # create factors from characters
  factor_vec <- as.factor(char_vec)
  # is an integer
  typeof(factor_vec)
  is.factor(factor_vec)
  levels(factor_vec)

  # integer type
  int_vec <- c(1L, 2L, 3L)
  is.numeric(int_vec)
  typeof(int_vec)
  levels(int_vec)

  # boolean or logical 
  bool_vec <- c(TRUE, FALSE, TRUE, FALSE)
  is.logical(bool_vec)

  # can coerce numeric to logical vector
  as.logical(vec)
```

Vectors of the same primitive type can be concatenated.

```
vec2 <- c(1,2,3, int_vec)
```

The missing value symbol in `R` is `NA`.
```
is.na(NA)

```

### Subset an atomic vector

Vector indexing begins with 1, and elements can be accessed using the bracket operator(s "[]"

```
char_vec[1:2]
char_vec[-3]

```


Various meta data such as attributes and names can be associated with atomic vectors allowing access by name:

```
names(char_vec)
names(char_vec) <- c("a", "b", "c")
names(char_vec)
char_vec["a"]

```

_todo: matrices, arrays_

Many base functions in `R` act on vectors  by default. 

A few examples of useful functions:

```
median(vec)
mean(vec)
sum(vec)
var(vec)
sd(vec)
max(3:10)
min(5:10)
range(1:20)

```


### Lists

Lists can contain any of the above types or other lists. 

```
alist <- list(doubles = vec, chars = char_vec, 
              factors = factor_vec, bools = bool_vec)

```

Access elements by name using the `$` operator or by using a double bracket operator ``[[]]``:

```
alist$factors
alist[[3]]
alist[["factors"]]
```

### Data frames

Data frames are the most common table data structure in `R`. Data frames are built on the list data structure and can be thought of as a list containing atomic vectors of all the same length. 

A simple example of a data frame: 

```
df <- data.frame(nums = 16:26, chars = letters[16:26])
head(df, 5)
tail(df, 5)
summary(df)
str(df)
```

The commands `head(),tail()` show the first/last elements of data frame, while `summary()` provides information about the type of variables an numerical summaries.

Elements of a data frame can be accessed by row and column number using the `[]` operator or columns can be accessed with `$`, as you would with a list.

``` 
df$chars
mean(df$nums)
df[2, ]
df[ ,2]
df[1,2]
```

The first number indicates the row and the second the column. Select all rows or columns by leaving that element blank. 

Elements can also be selected by number or logical vector

```
rows <- c(1,5,10,11)
df[rows, ]
# select all but those rows
df[-rows, ]

dim(df)
dim(df)[1]
nrow(df)
ncol(df)
```

In this example the base function `sample()` is used to create a vector of booleans which is used to select elements from the daa frame.

```
# sample random rows from the data frame
rows <- sample(c(TRUE,FALSE), nrow(df), replace = TRUE)
df[rows, ]
```

# Importing data

There are number methods for reading in data that are variations on the `read.table()`:

* `read.csv()`   : comma-separated files
* `read.csv2()`  : semi-colon-separated files
* `read.tsv()`   : tab-separated files
* `read.delim()` : general character-delimted files

The `tidyverse` equivalents can be found in the `readr` package and are named simliarly but usean underscore in place of a "." in the function name. (The `readr` functions have nice default options
and more explicit ways of checking how files are parsed.)


The default settings for `read.csv()`. 

```
read.csv(file, header = TRUE, sep = ",", quote = "\"",
              dec = ".", fill = TRUE, comment.char = "", ...)
```
To see the full set of defaults see the `read.table()`
function settings. 

The `header` determines if a header row is expected, 
if not default headers are automatically edited. 

Note that the default is to read strings in 
as characters. You can change this with the
`as.is` or `stringsAsFactors` options.


Here the string `data.csv` is imported as if it were read from 
a file. 

```
data.csv <- "col1,col2,col3
                       1,2,b 
                       3,4,c"
df <- read.csv(text = data.csv)

summary(df)
levels(df$col3)

df <- read.csv(text = data.csv, 
               header = TRUE, 
               row.nameds = FALSE, 
               as.is = TRUE)
summary(df)
```

The `na.strings` option can be used to indicate a vector of strings that should be interpreted as `NA` values.

You can specify column classes with `colClasses` and row and col names with `row.names, col.names`. 


# Manipulating data

For this section we'll use the `dplyr` package along with the `flights` data from the package `nycflights13`. 

Examples taken from [R for Data Science](http://r4ds.had.co.nz/)


```
install.packages("nycflights13")
library(dplyr)
library(nycflights13)

flights

```

The data frame is a 'tibble' essentially a data frame with a few features that differ from a regular data frame. For the most part it can be treated as a data frame. 


The `dplyr` package uses a few functions to access, filter, and mutate data. These functions are: 

1. `filter()`     : choose observations by value
2. `arrange()`    : reorder rows
3. `select()`     : choose variables by their names
4. `mutate()`     : create new variables from existing ones
5. `summarise()`  : collapse multiple values into a summary
6. `group_by()`   : operates on dataset by factors

The functions have a similar format: 

data frame + operation -> new data frame

Examples:

```

filter(flights, month ==1, day ==1)

```

*Note*

Logical operators in `R`:

`<, >, <=, >=, !=, ==`

These can be combined with
Logical and: `&`

Logical or: `|`

```
jan_late <- filter(flights, month ==1 & sched_arr_time > 2000)

```

Examples of other transformations:

```
# sort using multiple variables
arrange(flights, dep_delay, desc(carrier))
# select columns
select(flights, day, month)
select(flights -(year:day))
# partial match on column name
select(flights, ends_with("time"))
# rename column
rename(flights, departure_time = dep_time)
# create new varaibles
flights_speed <- mutate(flights, speed = distance/air_time *60)
hist(flights_speed$speed)
# keep only the new variables
transmutate(flights, speed = distance/air_time *60)
```

### Group_by and Summarise

The `summarise()` commands collapses a
data frame to a single row after applying
some summary function. 


```
summarise(flights_speed, avg_speed = mean(speed, na.rm = TRUE))

```

This becomes more useful when combined with
`group_by`, which can group summary data by 
some factor variable. 

```
by_carrier <- group_by(flights_speed, carrier)
summarise(flights_speed, avg_speed = mean(speed, na.rm = TRUE))

```


### The pipe operator

Expressions containing multiple operations can become confusing or repetitive. The pipe operator, `%>%` makes performing mulitple tranformations easier. 


The `na.rm` command removes `NA`'s from the computation.

```
library(knitr)

speed_table <- flights %>% 
                 mutate(speed = distance/air_time * 60) %>%
                 group_by(carrier) %>% 
                 summarise(avg_speed = mean(speed, na.rm = TRUE)) %>%
                 knitr::kable(format = "html")

```

The commands are 'piped' through the various functions and the final command returns an html table. The file can be saved and opened with a browser and copied to another document (like Word). You can outuput to different formats, such as markdown. 

(I would recommend learning markdown -- it's a convienent and simple 'markup' language that can be used to create html or other formatted documents. )

```
getwd()
setwd("/absolute/path/to/directory")
# write plain text to current directory 
cat(speed_table, file ="cat.html")
  
```
The `cat` command writes to a the stated file. Alternatively, you can specify the file name as an absolute path to the file. 


# Plotting with `ggplot2`

If you haven't loaded the `ggplot2` then do that now using `library(ggplot2)`. 

The `diamonds` and `mpg` datasets come with the `ggplot2` package. 

The basic syntax for `ggplot2` plots. Variables are mapped to 
aesthetic elements with `aes` and `geoms` are types of 
geometric elements that can be added to a plot. 

``` 
 ggplot(data = <data>, mapping = aes(<global mappings)) +
   <geom_function>(mapping = aes(<local mappings>,
                   stat = <stat>
                   position = <position>)) + 
                   
```

Here are some basic examples of plots for histograms, barplots and 
box plots using the `ggplot2` syntax. 



```
library(ggplot2)
head(diamonds)

# to see description of the datset
?diamonds

# barplot 
ggplot(data = diamonds) +
  geom_bar(mapping = aes(x = cut))

# histogram
ggplot(data = diamonds) +
  geom_histogram(aes(x = carat), binwidth = 0.5)


# compare variation in price by cut using geom_freqpoly
ggplot(data = diamonds, mapping = aes(x = price)) + 
  geom_freqpoly(aes(colour = cut), binwidth = 500)


# change position 
ggplot(data = diamonds, mapping = aes(x = cut, fill = color)) + 
        geom_bar(position = "dodge" )

# boxplot
ggplot(data = diamonds, mapping = aes(x = cut, y = price)) +
  geom_boxplot()

# reorder a variable
ggplot(data = mpg) +
  geom_boxplot(aes(x = reorder(class, hwy, FUN = median), y = hwy))

```


<!-- 

### Notes: To cover

* import
* tidy
* clean and transform
* visualize
* model 
 -->